import HelloWorld from "../HelloWorld.vue";

describe("accepts a value of msg", () => {
  it("playground", () => {
    cy.mount(HelloWorld, { props: { msg: "Hello Cypress" } });
    cy.get('[data-cy=foo]').should('contain', 'Hello Cypress')
  });

  it("shows the default if no msg is passed", () => {
    cy.mount(HelloWorld, { props: { msg: null } });
    cy.get('[data-cy=foo]').should('contain', 'foo')
  });
});
